# Домашнее задание по модулю "Дополнительные инструменты для работы с Docker" курса "DevOps. Docker"

## Цель практической работы
* Получить навык работы с Docker Hub, Docker Registry и DCT.

## Что нужно сделать
1. Разверните локально Docker Registry.
1. Найдите на [Docker Hub](https://hub.docker.com/) любой интересный вам образ.
1. Создайте на основе образа из пункта 2 свой образ. Присвойте образу тег.
1. Поместите ваш образ в Docker Registry.
1. Запушьте образ в Docker Hub.
1. **Опционально**: подпишите образ перед пулом в Docker Hub.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/docker/study_docker.git
   cd study_docker
   ```
   
1. Запускаем локальный Docker Registry
   ```shell
   docker-compose -f module_4/module_4.yml up -d
   ```
   
   ![Локальный Docker Registry](docs/imgs/run-docker-registry.png)

1. Собираем свой образ
   ```shell
   docker build -t module_4 ./module_4
   ```
   
   ![Собранный образ](docs/imgs/build-own-image.png)

1. Логинимся в локальном Docker Registry
   ```shell
   docker login http://localhost:5000
   ```
   
   ![Логин в локальном Docker Registry](docs/imgs/login-in-local-docker-registry.png)
   
1. Тэгаем свой образ и пушим в локальный Docker Registry
   ```shell
   docker tag module_4 localhost:5000/docker_module_4
   docker push localhost:5000/docker_module_4
   ```
   
   ![Тэг и пуш в локальный Docker Registry](docs/imgs/tag-and-push-in-local-docker-registry.png)

1. Логинимся в Docker Hub
   ```shell
   docker login
   ```
   
1. Тэгаем свой образ и пушим в Docker Hub
   ```shell
   docker tag module_4 vowatchka/skillbox_docker_module_4
   docker push vowatchka/skillbox_docker_module_4
   ```
   
   ![Тэг и пуш в локальный Docker Hub](docs/imgs/tag-and-push-in-docker-hub.png)

   Образ на Docker Hub

   ![Образ на Docker Hub](docs/imgs/own-image-in-docker-hub.png)

## Полезные материалы
* [Настройка частного реестра Docker в Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-18-04-ru)
* [Настройка локального хранилища Docker Registry](https://winitpro.ru/index.php/2021/03/03/nastrojka-lokalnogo-docker-registry/)
   