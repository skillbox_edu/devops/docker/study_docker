# Домашние задания курса "DevOps. Docker"

1. Основные компоненты Docker
1. [Базовые концепции Docker](module_2/README.md)
1. [Docker Builder и Dockerfile](module_3/README.md)
1. [Дополнительные инструменты для работы с Docker](module_4/README.md)
1. [Оркестрация контейнеров](module_5/README.md)
1. [Как работает Docker. Взгляд изнутри](module_6/README.md)