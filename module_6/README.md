# Домашнее задание по модулю "Как работает Docker. Взгляд изнутри" курса "DevOps. Docker"

## Цель практической работы
Получить навык конфигурации Docker и работы со Storage Driver.

## Что нужно сделать
1. Создайте конфигурационный файл для Dockerd.
1. Выберите три параметра для передачи в Dockerd через конфиг-файл.
1. Пропишите эти параметры в конфиг-файле.
1. Перезапустите сервис.
1. Найдите доказательства того, что конфигурационные параметры применились.
1. Сделайте скрин, на котором видно, что параметры в конфиг-файле или командной строке дали результат.
1. Прокиньте те же три параметра через флаги запуска Dockerd.
1. Измените текущий Storage Driver в Docker.
1. Сделайте скрины каталога текущего Storage Driver и его содержимого.
1. По желанию: создайте текстовый файл с описанием, как вы делали практическую работу.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/docker/study_docker.git
   cd study_docker
   ```
   
## Параметры dockerd, на которых будем проверять конфигурацию
* `bip` - IP-адрес сети `bridge`
  
   Посмотрим текущий IP-адрес сети `bridge`:
   ```shell
   docker network inspect -f {{.IPAM.Config}} bridge
   ```
  
  ![inspect bridge network ip](docs/imgs/inspect-bridge-network-ip.png)

* `icc` - разрешает межконтейнерное взаимодействие (по умолчанию `true`)

   Сначала убедимся, что межконтейнерное взаимодействие работает. Запустим первый контейнер:
   ```shell
   docker run --rm --name test1 -it busybox /bin/sh
   ```
  
   В соседнем терминале запустим еще один контейнер:
   ```shell
   export TEST1_IP=$(docker inspect -f {{.NetworkSettings.IPAddress}} test1)
   docker run --rm --name test2 -it -e IP=$TEST1_IP busybox ping $TEST1_IP -w 10
   ```
  
   ![inter-container-ping](docs/imgs/inter-container-ping.png)

* `metrics-addr` - адрес и порт по которому будут доступны метрики

  Запросим метрики:
  ```shell
  curl http://127.0.0.1:5000/metrics
  ```
  
  ![fail get metrics](docs/imgs/fail-get-metrics.png)

## Применение новой конфигурации docker с помощью конфига

1. Создаем конфиг `/etc/docker/daemon.json` с содержимым
   ```json
   {
     "bip": "172.17.0.10/16",
     "icc": false,
     "metrics-addr": "127.0.0.1:5000"
   }
   ```
   
1. Перезапускаем демона docker
   ```shell
   sudo systemctl restart docker
   ```

## Проверка новой конфигурации

1. Проверяем IP-адрес сети `bridge`
   ```shell
   docker network inspect -f {{.IPAM.Config}} bridge
   ```
   
   ![inspect bridge network ip again](docs/imgs/inspect-bridge-network-ip-again.png)

1. Проверяем межконтейнерное взаимодействие

   Запустим первый контейнер:
   ```shell
   docker run --rm --name test1 -it busybox /bin/sh
   ```
  
   В соседнем терминале запустим еще один контейнер:
   ```shell
   export TEST1_IP=$(docker inspect -f {{.NetworkSettings.IPAddress}} test1)
   docker run --rm --name test2 -it -e IP=$TEST1_IP busybox ping $TEST1_IP -w 10
   ```
   
   ![fail-inter-container-ping](docs/imgs/fail-inter-container-ping.png)

* Проверяем метрики

  Запросим метрики:
  ```shell
  curl http://127.0.0.1:5000/metrics
  ```
  
  ![get metrics](docs/imgs/get-metrics.png)

## Применение новой конфигурации docker с помощью параметров запуска

1. Можно также отредактировать параметры запуска в файле `/lib/systemd/system/docker.service` в настройке `[Service].ExecStart`
   ```ini
   ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --bip 172.17.0.10/16 --icc=false --metrics-addr 127.0.0.1:5000
   ```
   
1. Применяем изменения
   ```shell
   sudo systemctl daemon-reload
   ```

1. Перезапускаем демона docker
   ```shell
   sudo systemctl restart docker
   ```
   
## Изменение Storage Driver

1. Создаем конфиг `/etc/docker/daemon.json` с содержимым
   ```json
   {
     "storage-driver": "aufs"
   }
   ```
   
1. Перезапускаем демона docker
   ```shell
   sudo systemctl restart docker
   ```
   
1. Проверяем изменения
   ```shell
   docker info
   ```
   
   ![aufs storage driver](docs/imgs/aufs-storage-driver.png)

   Содержимое каталога нового Storage Driver

   ![aufs docker directory](docs/imgs/aufs-docker-directory.png)
   