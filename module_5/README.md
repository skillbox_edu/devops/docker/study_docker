# Домашнее задание по модулю "Оркестрация контейнеров" курса "DevOps. Docker"

## Цель практической работы
Составить свой compose-файл и запустить многоконтейнерное приложение при помощи Docker Compose.

## Что входит в практическую работу
1. Составить `Dockerfile`, файл `app.py` и, если требуется, другие файлы для сборки и запуска приложения. При помощи Docker Compose выполнить сборку и запуск приложения.
2. Добавить ещё один сервис в приложение из пункта 1. Запустить многоконтейнерное приложение при помощи Docker Compose.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/docker/study_docker.git
   cd study_docker
   ```
   
## Задание 1. Составление Dockerfile для сборки тестового веб-приложения на Python

1. Запускаем flask-приложение
   ```shell
   docker-compose -f module_5/compose.yml up -d
   ```
   
   ![docker compose up](docs/imgs/task-1-docker-compose-up.png)

1. Проверяем запущенное приложение

   ![запущенное приложение](docs/imgs/task-1-worked-app.png)

## Задание 2. Запуск многоконтейнерного приложения при помощи Docker Compose

1. Запускаем flask-приложение + redis
   ```shell
   docker-compose -f module_5/compose.yml up -d --build
   ```
   
   ![docker compose up --build](docs/imgs/task-2-docker-compose-up-rebuild.png)

1. Проверяем запущенное приложение

   ![запущенное приложение со счетчиком посещений](docs/imgs/task-2-seen-counter.png)

Контейнеры будут работать, даже если не описывать сеть в compose-файле и не пробрасывать порты для контейнера `redis`, потому что compose создает дефолтную сеть автоматически и подключает к ней контейнеры.
   