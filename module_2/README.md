# Домашнее задание по модулю "Базовые концепции Docker" курса "DevOps. Docker"

## Цель практической работы
* Получить навык работы с `Docker image`, `Docker volume`, `Docker container`.

## Что нужно сделать
1. Поднимите два или более контейнера с полезной нагрузкой на борту. 
1. Сделайте скрин со списком поднятых контейнеров.
1. Прокиньте порты на хост.
1. Убедитесь, что контейнеры пингуются из других. 
1. Сделайте скрин выдачи пинга.
1. Создайте сетку, которая полностью отключит взаимодействие контейнеров с сетью.
1. Приаттачьте сеть к контейнерам. Убедитесь, что это сработало. 
1. Сделайте скрин с неработающим, к примеру, apt update.
1. Приаттачьте к одному из контейнеров tmpfs-волюм.
1. Сделайте скрин с командой.
1. Создайте общий волюм для двух контейнеров. 
1. Добавьте в общий волюм полезную информацию. Например, логи либо ваши самописные скрипты. 
1. Сделайте скрины приаттаченного волюма и прокинутых в контейнер файлов/данных.
1. По желанию: создайте файл с текстовым описанием, что, как и почему вы делали.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/docker/study_docker.git
   cd study_docker
   ```

1. Поднимаем контейнеры

   ```shell
   docker run -d --name nginx nginx
   docker run -d --name prometheus prom/prometheus:v2.41.0
   ```

   ![Поднятые контейнеры](docs/imgs/1.png)

1. Поднимаем контейнеры с пробросом портов

   ```shell
   docker run -d -p 8080:80 --name nginx nginx
   docker run -d -p 9090:9090 --name prometheus prom/prometheus:v2.41.0
   ```
   
   ![Поднятые контейнеры с пробросом портов](docs/imgs/2.png)

1. Создаем сеть и объединяем в ней контейнеры

   ```shell
   docker network create module_2
   docker network connect module_2 nginx
   docker network connect module_2 prometheus
   ```
   
1. Устанавливаем в контейнер `nginx` утилиту `ping`.
   
   ```shell
   docker exec -it nginx /bin/bash
   root@5cfc04ee7445:/# apt-get update && apt-get install -y iputils-ping
   ```
   
1. Пингуем контейнер `prometheus` из контейнера `nginx`

   ```shell
   docker exec nginx ping prometheus
   ```
   
   ![Пинг контейнера prometheus из контейнера nginx](docs/imgs/3.png)
   
1. Пингуем контейнер `nginx` из контейнера `prometheus`

   ```shell
   docker exec -u 0 prometheus ping nginx
   ```
   
   ![Пинг контейнера nginx из контейнера prometheus](docs/imgs/4.png)

1. Отключаем сеть в контейнерах

   ```shell
   docker network disconnect module_2 nginx
   docker network disconnect module_2 prometheus
   docker network disconnect bridge nginx
   docker network disconnect bridge prometheus
   docker network connect none nginx
   docker network connect none prometheus
   ```
   
1. Поверяем, что сеть не работает

   ```shell
   docker exec nginx /bin/bash -c "apt-get update -y"
   ```
   
   ![Сеть не работает](docs/imgs/5.png)

1. Аттачим контейнеру tmpsf-вольюм

   ```shell
   docker run -d --name nginx_1 --tmpfs /mount nginx
   ```
   
   ![tmpsf-вольюм](docs/imgs/6.png)

1. Создаем вольюм и копируем в него файл

   ```shell
   docker volume create module_2_volume
   sudo cp -r /vagrant/docker/study_docker/module_2/data/* /var/lib/docker/volumes/module_2_volume/_data/
   ```
   
1. Поднимаем контейнеры с общим вольюмом

   ```shell
   docker run -d --name nginx_2 --mount type=volume,source=module_2_volume,destination=/mount nginx
   docker run -d --name prometheus_2 --mount type=volume,source=module_2_volume,destination=/mount prom/prometheus:v2.41.0
   ```
   
   ![Контейнер nginx с общим вольюмом](docs/imgs/7.png)

   ![Контейнер prometheus с общим вольюмом](docs/imgs/8.png)
