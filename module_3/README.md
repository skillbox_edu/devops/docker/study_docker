# Домашнее задание по модулю "Docker Builder и Dockerfile" курса "DevOps. Docker"

## Цель практической работы
* Вы научитесь составлять свой `Dockerfile` и собирать докер-образ с помощью концепции Docker Builder.

## Что входит в практическую работу
1. Составить `Dockerfile` для сборки тестового React-приложения
1. Собрать образ.
1. Запустить контейнер из собранного образа.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/docker/study_docker.git
   cd study_docker
   ```

### Задание 1. Составление Dockerfile для сборки тестового React-приложения

1. Если не установлен `nodejs`, то качаем [архив](https://nodejs.org/en/download/) и ставим по инструкции `Installing Node.js via binary archive`, расположенной в конце страницы с файлами `nodejs`.

1. Создаем react-приложение
   ```shell
   npx create-react-app ~/docker_module_3/my-app
   ```
   
1. Копируем файлы для сборки образа в `my-app`
   ```shell
   cp module_3/Dockerfile ~/docker_module_3/my-app/
   cp module_3/.dockerignore ~/docker_module_3/my-app/
   cp module_3/nginx.conf ~/docker_module_3/my-app/
   ```
   
1. Содержимое папки `my-app`

   ![Содержимое папки my-app](docs/imgs/task-1-ls-my-app.png)

   Файлы для образа:
   
   * [Dockerfile](Dockerfile)
   * [.dockerignore](.dockerignore)
   * [конфигурация nginx](nginx.conf)

### Задание 2. Сборка образа

1. Собираем образ
   ```shell
   docker build -t skillbox/module_3 ~/docker_module_3/my-app/.
   ```
   
   ![Вывод docker build](docs/imgs/task-2-docker-build.png)
   
1. Собранный образ весит в разы меньше промежуточного (`<none>`)
   ```shell
   docker images
   ```

   ![docker images](docs/imgs/task-2-docker-images.png)

### Задание 3. Запуск контейнера из собранного образа 

1. Запускаем контейнер
   ```shell
   docker run -d --rm --name skillbox_docker_module_3 -p 8080:80 skillbox/module_3
   ```
   
   ![Запущенный контейнер](docs/imgs/task-3-docker-run.png)
   
1. Смотрим логи запущенного контейнера
   ```shell
   docker logs skillbox_docker_module_3
   ```
   
   ![Логи запущенного контейнера](docs/imgs/task-3-docker-logs.png)

1. Открываем [react-приложение](http://127.0.0.1:8080/) в браузере

   ![React-приложение в браузере](docs/imgs/task-3-react-app.png)
   