import socket
from flask import Flask
from redis import Redis


app = Flask(__name__)
rediska = Redis(host="redis", port=6379)


@app.route("/")
def index():
    count = rediska.incr("hits")
    return (f"<h1>Hello from {socket.gethostname()}</h1>"
            f"<p>I have been seen {count} times</p>")
